package com.example.certuitclima.Contrato;

import com.example.certuitclima.Modelo.Clima;

import java.util.ArrayList;
import java.util.List;

public interface Contrato {

    interface Vista{
        void mostrarTempActual(Double tempActual);
        void mostrarForecast(ArrayList forecast);
        void mostrarHumedad(Double humedad);
        void mostrarCiudad(Clima ciudad);
        void mostrarNombre(String nombreCiudad);
        void getUbicacion(Double lat, Double lon);
        void getCiudadN(String nombre);
        void getCiudadZ(int zipCode);
        void conexion();
    }

    interface Presentador{
        void setVista(Vista vista);
        void setUbicacion(Double lat,Double lon);
        void setCiudadBuscar(String nombreCiudadBuscar);
        void obtenerClimaActual(String nomCiudad);
        void verificarConexion(boolean estado);
    }

}
