package com.example.certuitclima.Data.Retrofit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.certuitclima.Modelo.Clima;
import com.example.certuitclima.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestRetrofit extends Fragment {
    private TextView mJonTxtView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getClimas();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.mexicali_5dayclima, container, false);

        return view;
    }

    private void getClimas(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.openweathermap.org/data/2.5/").addConverterFactory(GsonConverterFactory.create()).build();

        JsonPlaceHolder jsonPlaceHolderAPI = retrofit.create(JsonPlaceHolder.class);

        Call<Ciudad> call = jsonPlaceHolderAPI.getCiudad();

        call.enqueue(new Callback<Ciudad>() {
            @Override
            public void onResponse(Call<Ciudad> call, Response<Ciudad> response) {
                if(!response.isSuccessful()){
                    Log.d("Retrofit","Error");
                }
                Ciudad clima = response.body();

                mJonTxtView.setText(clima.getName());
                Log.d ("Retrofit",clima.getName()+" <- ciudad del clima");
            }

                     @Override
            public void onFailure(Call<Ciudad> call, Throwable t) {
                         Log.d("Retrofit","Error de: " + t.getMessage());
            }
        });
    }
}
