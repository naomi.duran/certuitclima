package com.example.certuitclima.Data.Retrofit;

import com.example.certuitclima.Modelo.Clima;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolder {

    @GET("weather?q=Mexicali&units=metric&APPID=30abf562d9555a742c6a5f7a9ca94295")
    Call<Ciudad> getCiudad();
}
