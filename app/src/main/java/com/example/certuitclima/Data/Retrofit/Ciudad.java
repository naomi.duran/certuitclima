package com.example.certuitclima.Data.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Ciudad {
    @SerializedName("coord")
    private Double coord[];
    @SerializedName("weather")
    private ArrayList weather;
    @SerializedName("base")
    private String base;
    @SerializedName("main")
    private Double main[];
    @SerializedName("visibility")
    private int visibility;
    @SerializedName("wind")
    private int [] wind;
    @SerializedName("clouds")
    private int [] clouds;
    @SerializedName("dt")
    private long dt;
    @SerializedName("sys")
    private ArrayList sys;
    @SerializedName("timezone")
    private long timezone;
    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
    @SerializedName("cod")
    private int cod;






    public Double[] getCoord() {
        return coord;
    }

    public void setCoord(Double[] coord) {
        this.coord = coord;
    }

    public ArrayList getWeather() {
        return weather;
    }

    public void setWeather(ArrayList weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Double[] getMain() {
        return main;
    }

    public void setMain(Double[] main) {
        this.main = main;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int[] getWind() {
        return wind;
    }

    public void setWind(int[] wind) {
        this.wind = wind;
    }

    public int[] getClouds() {
        return clouds;
    }

    public void setClouds(int[] clouds) {
        this.clouds = clouds;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public ArrayList getSys() {
        return sys;
    }

    public void setSys(ArrayList sys) {
        this.sys = sys;
    }

    public long getTimezone() {
        return timezone;
    }

    public void setTimezone(long timezone) {
        this.timezone = timezone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
