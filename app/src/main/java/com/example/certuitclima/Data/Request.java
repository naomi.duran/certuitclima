package com.example.certuitclima.Data;

import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.certuitclima.Modelo.Clima;
import com.example.certuitclima.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Request {

    private RequestQueue queue;
    private JSONObject ciudad,dia;
    private Double tempMiCiudad,tempA,tempMx,temMn;
    private TextView temp,dia1,dia2,dia3,dia4,dia5;
    private JSONArray diasForecast,forecast;
    private OnRequestCompletedListener mRequestCompletedListener;
    private RequestQueue requestQueue;
    private String APIKEY = "30abf562d9555a742c6a5f7a9ca94295";
    private Clima climaActual;



    public void obtenerTempA(String nombreC,String nombrePais){
        climaActual.setCiudad(nombreC);
        String url = "https://api.openweathermap.org/data/2.5/weather?q="+ nombreC + ","+ nombrePais +"&units=metric&APPID="+ APIKEY;

        JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    ciudad = response.getJSONObject("main");
                    tempMiCiudad = ciudad.getDouble("temp");
                    climaActual.setTemperatura(tempMiCiudad);
                    climaActual.setTempMax(ciudad.getDouble("temp_max"));
                    climaActual.setTempMin(ciudad.getDouble("temp_min"));
                    climaActual.setHumedad(ciudad.getDouble("humidity"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               /* try {
                    ciudad = response.getJSONObject("main");
                    tempMiCiudad = ciudad.getDouble("temp");

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Request Class","Ocurrió un error en el request");
                }*/

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError) {
                    //This indicates that the reuest has either time out or there is no connection
                    Log.d("Request Class","Error de timeout");
                } else if (error instanceof AuthFailureError) {
                    // Error indicating that there was an Authentication Failure while performing the request
                    Log.d("Request Class","Error de auth");
                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    Log.d("Request Class","Error de respuesta del server");
                } else if (error instanceof NetworkError) {
                    //Indicates that there was network error while performing the request
                    Log.d("Request Class","Error de error de conexión");
                } else if (error instanceof ParseError) {
                    // Indicates that the server response could not be parsed
                    Log.d("Request Class","Error de tipo de dato parsed");
                }
            }
        });

        queue.add(request);
    }


    private void obtenerForecast(){
        String url = "https://api.openweathermap.org/data/2.5/forecast?q=Mexicali,Mexico&units=metric&APPID=30abf562d9555a742c6a5f7a9ca94295";


        JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Request class","Entre al onResponse");
                try {
                    forecast = response.getJSONArray("list");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int x=0;x<=39;x++){
                    switch(x){
                        case 6:

                            break;
                        case 15:

                            break;
                        case 23:

                            break;
                        case 33:

                            break;
                        case 39:

                            break;
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError) {
                    //This indicates that the request has either time out or there is no connection
                    Log.d("Request class","Error de timeout");
                } else if (error instanceof AuthFailureError) {
                    // Error indicating that there was an Authentication Failure while performing the request
                    Log.d("Request class","Error de auth");
                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    Log.d("Request class","Error de respuesta del server");
                } else if (error instanceof NetworkError) {
                    //Indicates that there was network error while performing the request
                    Log.d("Request class","Error de error de conexión");
                } else if (error instanceof ParseError) {
                    // Indicates that the server response could not be parsed
                    Log.d("Request class","Error de tipo de dato parsed");
                }
            }
        });

        queue.add(request);



    }






    public interface OnRequestCompletedListener {
        /**
         * Called when the String request has been completed.
         *
         * @param requestName  the String refers the request name
         * @param status       the status of the request either success or failure
         * @param response     the String response returns from the Webservice. It may be
         *                     null if request failed.
         * @param errorMessage the String refers the error message when request failed to
         *                     get the response
         */
        void onRequestCompleted(String requestName, boolean status,
                                JSONObject response, String errorMessage);

    }

    public Clima getClimaActual() {
        return climaActual;
    }
}

