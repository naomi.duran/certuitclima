package com.example.certuitclima.Vista;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.certuitclima.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;

public class Busqueda extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "Navegador";
    private RequestQueue queue;
    private GoogleMap mMap;
    private JSONObject ciudad;
    private LatLng punto;

    public Busqueda(){

    }

    public static Busqueda newInstance(String param1, String param2) {
        Busqueda fragment = new Busqueda();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_busqueda, container, false);

        return view;
    }
   /* protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setEnabled(true);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync( this);
    }
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {


            @Override
            public void onMapLongClick(LatLng arg0) {
                mMap.clear();


                Double lati = (arg0.latitude);
                Double loni = (arg0.longitude);
                LatLng punto = new LatLng(lati,loni);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(punto));
                buscarClima(lati,loni);

                Log.d(TAG,String.valueOf(lati));
                Log.d(TAG,String.valueOf(loni));


            }
        });
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
                    Toast.makeText(Busqueda.this, "Humedad:"+ciudad.getDouble("humidity")+"%\nTemp Max:"+ciudad.getDouble("temp_max")+" Temp Min:"+ciudad.getDouble("temp_min"),
                            Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
*/


    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}


