package com.example.certuitclima.Vista;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.certuitclima.Contrato.Contrato;
import com.example.certuitclima.Modelo.Clima;
import com.example.certuitclima.R;

import java.util.ArrayList;

import javax.inject.Inject;

public class PantallaPrincipal extends Fragment implements Contrato.Vista {
    @Inject Contrato.Presentador presentador;

    private TextView nombreC,
    tempActua,
    tempMax,
    tempMin,
    humed;
    private boolean status;

    public PantallaPrincipal(){

    }


    public static PantallaPrincipal newInstance(String param1, String param2) {
        PantallaPrincipal fragment = new PantallaPrincipal();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    //    presentador.setVista(this);

        cargarVista();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_activity, container, false);

        return view;
    }
    public void cargarVista(){
       //nombreC = findViewById(R.id.miCiudad);
      // tempActua = findViewById(R.id.tempActualMexicali);
      // humed = findViewById(R.id.tempH);


        presentador.obtenerClimaActual("Mexicali");

    }




    @Override
    public void mostrarTempActual(Double tempActual) {
            tempActua.setText(String.valueOf(tempActual));
    }

    @Override
    public void mostrarForecast(ArrayList forecast) {

    }

    @Override
    public void mostrarHumedad(Double humedad) {
            humed.setText(String.valueOf(humedad));
    }

    @Override
    public void mostrarCiudad(Clima ciudad) {
        tempActua.setText(String.valueOf(ciudad.getTemperatura()));
    }

    @Override
    public void mostrarNombre(String nombreCiudad) {
        nombreC.setText(nombreCiudad);
    }

    @Override
    public void getUbicacion(Double lat, Double lon) {

    }

    @Override
    public void getCiudadN(String nombre) {

    }

    @Override
    public void getCiudadZ(int zipCode) {

    }

    @Override
    public void conexion() {
        presentador.verificarConexion(status);
    }


}

