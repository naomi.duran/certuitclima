package com.example.certuitclima.Vista;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;

import com.example.certuitclima.Data.Retrofit.RequestRetrofit;
import com.example.certuitclima.R;

public class Navegador extends AppCompatActivity  {


    private static final String TAG = "Navegador";


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {

                case R.id.top:
                    fragment = new TopCiudadesActivity();
                    loadFragment(fragment);
                    return true;
                case R.id.principal:
                    fragment = new PantallaPrincipal();
                    loadFragment(fragment);
                    return true;
                case R.id.busqueda:
                    fragment = new Busqueda();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal_fragment);
        BottomNavigationView navView = findViewById(R.id.navegador);
        navView.setEnabled(true);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new PantallaPrincipal());
    }


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
