package com.example.certuitclima.Presentador;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.certuitclima.Contrato.Contrato;
import com.example.certuitclima.Data.Request;
import com.example.certuitclima.Modelo.Clima;

import org.json.JSONObject;

public class Presentador implements Contrato.Presentador,Request.OnRequestCompletedListener {
    private Contrato.Vista vista;
    private Request.OnRequestCompletedListener listener;
    private JSONObject objetoRespuesta;
    private Request req;




    @Override
    public void setVista(Contrato.Vista vista) {
        this.vista = vista;
    }

    @Override
    public void setUbicacion(Double lat, Double lon) {
            vista.getUbicacion(lat,lon);
    }

    @Override
    public void setCiudadBuscar(String nombreCiudadBuscar) {
                Clima ciudadActual = null;

                ciudadActual.setCiudad(nombreCiudadBuscar);

    }

    @Override
    public void obtenerClimaActual(String nomCiudad) {

        req.obtenerTempA(nomCiudad, "Mexico");
        vista.mostrarTempActual(req.getClimaActual().getTemperatura());
    }

    @Override
    public void verificarConexion(boolean estado) {

      //  ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

      //  NetworkInfo netInfo = cm.getActiveNetworkInfo();

      // estado = netInfo != null && netInfo.isConnectedOrConnecting();
    }


    @Override
    public void onRequestCompleted(String requestName, boolean status, JSONObject response, String errorMessage) {
                    objetoRespuesta = response;
    }
}
