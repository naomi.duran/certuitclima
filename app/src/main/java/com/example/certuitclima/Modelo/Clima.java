package com.example.certuitclima.Modelo;

import java.util.ArrayList;
import java.util.List;

public class Clima {
    String ciudad="";
    double temperatura=0.00,tempMax=0.00,tempMin=0.00,humedad=0.00;

    public Clima(String nombreC, Double t, Double tmx,Double tmn, Double h){
        this.ciudad = nombreC;
        this.temperatura= t;
        this.tempMax = tmx;
        this.tempMin = tmn;
        this.humedad = h;
    }

    public Clima(String nombreC, Double t){
        this.ciudad = nombreC;
        this.temperatura= t;

    }

    public  Clima(){

    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getHumedad() {
        return humedad;
    }

    public List<Clima> getClimas() {
        return climas;
    }

    public void setHumedad(double humedad) {
        this.humedad = humedad;
    }

    private List<Clima> climas;

    // This method creates an ArrayList that has three Person objects
// Checkout the project associated with this tutorial on Github if
// you want to use the same images.
    private void initializeData() {
        climas = new ArrayList<>();
        climas.add(new Clima("Mexico D.F", 23.00,36.00,15.00,10.00));
        climas.add(new Clima("Monterrey", 25.00,36.00,15.00,10.00));
        climas.add(new Clima("Sonora", 35.00,36.00,15.00,10.00));
    }
}